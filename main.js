
function fabriquerPlateau(maxLine,maxSquare){ 
    
    //implantation de ma fonction 

    /**
     * mettre en place le plateu d'un jeu d'echec grille de 8x8 avec des couleurs alternées
     *  - une case d'une couleur ne peut pas être a coté d'une case d'une même couleur (cad une case d'une couleur différente).
     * 
     * evidemment on part d'une grille de 8X8 mais rien ne nous empèche d'utiliser ce code pour generer par exemple des grille de sudoku, de mots croisées , etc... donc la taille peut varier.)
     * d'où l'utilisation d'un algo... SOUPLE.
     * 
     * |1|0|1|0|1|0|1|0|
     * |0|1|0|1|0|1|0|1|
     * |1|0|1|0|1|0|1|0|
     * |0|1|0|1|0|1|0|1|
     * |1|0|1|0|1|0|1|0|
     * |0|1|0|1|0|1|0|1|
     * |1|0|1|0|1|0|1|0|
     * |0|1|0|1|0|1|0|1|
     */



    // les limites maximales de ma grille
    /* il n'y a pas besoin de déclarer les variables maxLine et maxSquare car ils sont paramétrés dans la déclaration de la fonction
    let maxLine = 8; // le nombre maximal de ligne de la grille
    let maxSquare = 8; // le nombre de case par ligne
    */


    // variable definisant la valeur a mettre dans la case
    let value = true; // true definissant la couleur blanche.

    // initialiser la grille (le plateau)
    let grid = [];


    // ***FABRICATION DE LA GRILLE***

    /**
     * boucle qui va fabriquer notre plateau (fabrication DES lignes)
     * utilisation de maxLines pour definir les lignes restantes a fabriquer
     */
    while(maxLine > 0){

        /**
         * fabrique ligne
         */
        let line = []; // initialisation de la ligne
        for(let i = 0 ; i < maxSquare ; i++){
            // fabrication effective de la ligne
            line[i] = value; // on insere la valeur dans la bonne case de la ligne

            if(i < maxSquare - 1) value = !value;  //on change la valeur (2 états posibles)
        }

        //ajoute la ligne à notre plateau
        grid.push(line);

        // reduit de 1 le nombre de lignes a fabriquer
        maxLine--; 
    }

return grid; //on retourne le résultat de la fonction


}
let plateau = fabriquerPlateau(8,8); //j'appelle ma fonction
console.log(plateau); //affichage du plateau en true et false avec ma fonction


// ***AFFICHAGE DE LA GRILLE ( Rendu des données ;) )***

// console.log(grid); //ne peux plus être affiché avec la fonction car grid est devenue le résultat 



function afficherPlateau(grid){ 
    
    // implantation de ma fonction
// Initialisation des elements de l'affichage
let white = ' ';
let black = 'X';
let separator = '|';

// itération sur les lignes
for(let i = 0 ; i < grid.length ; i++){

    //initialisation de la ligne a afficher
    let line = separator; //ma ligne commence avec | (separator)


    // recuperer la valeur de chaque case de la ligne pour faire mon affichage
    for(let j = 0 ; j < grid[i].length ; j ++ ){
        let currentSquare = grid[i][j];
        // la condition ne gère que le caractère a afficher qui depend de la valeur de la case (ce qu'il y a entre les separateurs)
        if(currentSquare){
            line +=  white; // line = line + white;

        }else{
            line +=  black;
        }
        // le séparateur est ajouter tout le temps
        line += separator;

        // algo alternatif a base de ternaire (equivalent à la partie du dessus)
            // let character = (currentSquare)?white:black; // utilisation du ternaire (character prend la valeur de white ou de black selon currentSquare)
            // line += character + separator;        

    }

    // afficher la ligne sur la sortie (interface graphique)
    console.log(line);

}
}
let echequier = afficherPlateau(plateau);//j'appelle ma fonction


// Fabrication d'un deuxième plateau
let plateau2;
let echequier2;

plateau2 = fabriquerPlateau(10,10);
echequier2 = afficherPlateau(plateau2);

